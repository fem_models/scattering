A Onelab model for 3D scattering problems in nanophotonics.

## Synopsis

This project contains a [Onelab](http://onelab.info/wiki/ONELAB) model for solving various 3D electromagnetic problems on an isolated arbitrary object:
* T-matrix computation [1]
* Quasi-normal modes
* Plane wave response
* Green's function (e.g. to compute the Local Density Of States)

It contains various usefull features in electromag:
* Vector partial waves (AKA vector spherical harmonics)
* Total field formuation with a point source illumination ("an oriented delta")
* Linear eigenvalue problems
* Scattered field formulation
* Spherical PMLs


## Installation

This model requires the following programs:
* [gmsh](http://www.gmsh.info/)
* [getdp](http://www.getdp.info/) compiled with python support (see below)
* python (>3.5.x) with numpy, scipy and matplotlib

## Running the model

Open `scattering.pro` with Gmsh.
The default parameters are set to compute the T-matrix of a sphere. It retrieves the results from [1].

## Authors

Guillaume Demésy and Brian Stout

## References
[1] See "[Scattering matrix of arbitrarily shaped objects: Combining Finite Elements and Vector Partial Waves](https://arxiv.org/abs/1802.00596)" for details about T-matrices

## Installation notes
To enable python support (Python[] function) in GetDP, follow [these instructions (with complex arithmetic)](https://gitlab.onelab.info/getdp/getdp/wikis/GetDP-compilation) and add to the final cmake line:

`-DENABLE_PYTHON=ON -DPYTHON_LIBRARY=/path/to/pythonlib -DPYTHON_INCLUDE_DIR=/path/to/pythoninclude`
*  On Debian/Ubuntu systems, for python3.6 installed with apt-get,
    * `/path/to/pythonlib` is `/usr/lib/x86_64-linux-gnu/libpython3.6m.so`
    * `/path/to/python/include` is `/usr/include/python3.6m`
* For python versions installed through anaconda in some environment (e.g. env py36 below), a common location is:
    * `/somepath/anaconda3/envs/py36/lib/libpython3.6m.so`
    * `/somepath/anaconda3/envs/py36/include/python3.6m`

